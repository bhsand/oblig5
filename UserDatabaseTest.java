
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class UserDatabaseTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class UserDatabaseTest
{
    /**
     * Default constructor for test class UserDatabaseTest
     */
    public UserDatabaseTest(){}

    @Test
    public void testDatabase() {
        UserDatabase instance = UserDatabase.getInstance();
        Student sample1 = new Student("Forste;Testperson\tINFO100;INFO200;");
        instance.addStudent(sample1);
        Student sample2 = new Student("Andre;Testperson\tINFO200;INFO300;");
        instance.addStudent(sample2);
        Student sample3 = new Student("Testmann;Tre\tINFO400;INFO500;");
        instance.addStudent(sample3);
        Student sample4 = new Student("Testdame;Fire\tINFO600;INFO700;INFO800;");
        instance.addStudent(sample4);
        instance.saveDatabase();
        instance.listStudents();
        //instance.getStudents().clear();
    }

}
