import java.util.ArrayList;

/**
 * Write a description of class Student here.
 *
 * @author Bjørn Helge Sandblåst
 * @version 1.0
 */
public class Student
{
    private Name name;
    private CourseCollection courses;

    public Student(String firstName, String surname) {
        name = new Name(firstName, surname);
        courses = new CourseCollection();
    }

    public Student(String encodedStudent) {
        if(encodedStudent.contains("\t")) {
            String[] parts = encodedStudent.split("\t");
            name = new Name(parts[0]);
            if (parts[1] != null) {
                courses = new CourseCollection(parts[1]);
            }
        }
        else { 
            throw new IllegalArgumentException("Error while creating Student object.");
        }
    }

    public Name getName() {
        return name;
    }

    public CourseCollection getCourses() {
        return courses;
    }

    public void addCourse(String courseCode) {
        courses.addCourse(courseCode);
    }

    public String encode() {
        return name.encode()+"\t"+courses.encode();
    }

}
