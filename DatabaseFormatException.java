import java.io.IOException;

/**
 * Write a description of class DatabaseFormatException here.
 *
 * @author Bjørn Helge Sandblåst
 * @version 1.0
 */
public class DatabaseFormatException extends Exception
{
    private static final long serialVersionUID = 7526472295622776147L; // unique id

    public DatabaseFormatException(String result) {
     super(result + " kan ikke dekodes");   
    }
}
