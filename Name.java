
/**
 * Write a description of class Name here.
 *
 * @author Bjørn Helge Sandblåst
 * @version 1.0
 */
public class Name
{
    private String firstName;
    private String surname;

    public Name(String firstName, String surname) {
        this.firstName = firstName;
        this.surname = surname;
    }

    public Name(String encodedIdentity) {
        String[] parts = encodedIdentity.split(";");
        firstName = parts[0];
        surname = parts[1];
    }

    public String getFirstName(){
        return firstName;
    }

    public String getSurname(){
        return surname;
    }

    public String encode() {
        return firstName + ";" + surname;
    }
}
