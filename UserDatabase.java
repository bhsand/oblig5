import java.io.*;
import java.util.*;
import java.nio.file.*;
import java.util.stream.Collectors;
import java.util.ArrayList;

/**
 * Write a description of class UserDatabase here.
 * 
 * @author Bjørn Helge Sandblåst
 * @version 1.0
 */
public class UserDatabase
{
    public static final String DATABASE_FILENAME = "DB.tsv";
    public static final Path DATABASE_PATH = Paths.get(DATABASE_FILENAME);
    private ArrayList<Student> students;
    private static UserDatabase instance;

    private UserDatabase() {
        students = new ArrayList<Student>();
    }

    public static UserDatabase getInstance() {
        if(instance == null) {
            instance = new UserDatabase();
        }
        return instance;
    }

    public ArrayList<Student> getStudents(){
        return students;   
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    public void listStudents() {
        for (Student student : students) {
            System.out.println(student.getName().getFirstName() + " " + student.getName().getSurname());
        }
    }

    public boolean saveDatabase(){
        try {
            FileWriter writer = new FileWriter(DATABASE_FILENAME);
            for(Student student : students) {
                writer.write(student.encode() + "\n");  
            }
            writer.close();
            return true;
        }
        catch(Exception e) {
            System.out.println("Noe gikk galt");
            return false;
        }
    }

    public boolean loadDatabase() throws Exception {
        BufferedReader reader = Files.newBufferedReader(DATABASE_PATH);
        String response = reader.readLine();
        while(response != null) {
            if(!response.contains(";")) {
                DatabaseFormatException dfe;
                dfe = new DatabaseFormatException(response);
                throw dfe;
            }
            Student studentObject = new Student(response);
            students.add(studentObject);
            response = reader.readLine();
        }
        reader.close();
        return true;
    }
}
