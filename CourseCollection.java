import java.util.ArrayList;

/**
 * Write a description of class CourseCollection here.
 *
 * @author Bjørn Helge Sandblåst
 * @version 1.0
 */
public class CourseCollection
{
    private ArrayList<String> courses;

    public CourseCollection(){
        courses = new ArrayList<>();
    }

    public CourseCollection(String coursesEncoded){
        courses = new ArrayList<String>();
        String[] parts = coursesEncoded.split(";");
        for(String part : parts){
            courses.add(part);
        }
    }

    public String encode() {
        String result = "";
        for(String part : courses) {
            result = result+part + ";";
        }
        return result;
    }

    public ArrayList<String> getCoursesArray() {
        return courses;
    }

    public void addCourse(String courseCode) {
        if(courseCode==null || courseCode.contains(";")){
            throw new IllegalArgumentException(courseCode + " is an invalid course code");
        }
        assert(!courses.contains(courseCode)):courseCode + " is already added";
        courses.add(courseCode);
    }
}
